public class Board {
    private Square[][] tictactoeBoard;
    public Board(){
        tictactoeBoard=new Square[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tictactoeBoard[i][j]=Square.BLANK;
            }
        }
    }
    public String toString(){
        String board="";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board+=tictactoeBoard[i][j]+" ";
            }
            board+="\n";
        }
        return board;
    }
    public boolean placeToken(int row, int col, Square playerToken){
        if(!(row<=3&&row>=0&&col<=3&&col>=0)){
            return false;
        }else if(tictactoeBoard[row-1][col-1]!=Square.BLANK){
            return false;
        }else{
            tictactoeBoard[row-1][col-1]=playerToken;
            return true;
        }
    }
    public boolean checkIfFull(){
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if(tictactoeBoard[i][j]==Square.BLANK){
                    return false;
                }
            }
        }
        return true;
    }
    private boolean checkIfWinningHorizontal(Square playerToken){
        for (int i = 0; i < 3; i++) {
            if(tictactoeBoard[i][0]==playerToken&&tictactoeBoard[i][1]==playerToken&&tictactoeBoard[i][2]==playerToken){
                return true;
            }
        }
        return false;
    }
    private boolean checkIfWinningVertical(Square playerToken){
        for (int i = 0; i < 3; i++) {
            if(tictactoeBoard[0][i]==playerToken&&tictactoeBoard[1][i]==playerToken&&tictactoeBoard[2][i]==playerToken){
                return true;
            }
        }
        return false;
    }
    private boolean checkIfWinningDiagonal(Square playerToken){
        if(tictactoeBoard[0][0]==playerToken&&tictactoeBoard[1][1]==playerToken&&tictactoeBoard[2][2]==playerToken){
            return true;
        }else return tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][0] == playerToken;
    }
    public boolean checkIfWinning(Square playerToken){
        return checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningDiagonal(playerToken);
    }
}
