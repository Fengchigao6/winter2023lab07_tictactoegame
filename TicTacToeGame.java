import java.util.Scanner;
public class TicTacToeGame {
    public static void main(String[] args) {
        System.out.println("Welcome to Tic-Tac-Toe Game");
        System.out.println("Player 1's token: X");
        System.out.println("Player 2's token: O");
        Board board =new Board();
        boolean gameOver=false;
        int player=1;
        Square playerToken=Square.X;
        Scanner sc=new Scanner(System.in);
        while (!gameOver){
            System.out.println(board);
            if(player==1){
                playerToken=Square.X;
            }else{
                playerToken=Square.O;
            }
            System.out.println("Player"+player+", please enter your row");
            int row = sc.nextInt();
            System.out.println("Player"+player+" please enter your column");
            int col =sc.nextInt();
            while(!board.placeToken(row,col,playerToken)){
                System.out.println("Player"+player+ " ,please re-enter a row");
                row= sc.nextInt();
                System.out.println("Player"+player+" ,please re-enter a column");
                col = sc.nextInt();
            }
            if(board.checkIfWinning(playerToken)){
                System.out.println("Player "+player+" wins!");
                gameOver=true;
            }else if(board.checkIfFull()){
                System.out.println("It's a tie!");
                gameOver=true;
            }else{
                player+=1;
                if(player>2){
                    player=1;
                }
            }
        }
    }
}
